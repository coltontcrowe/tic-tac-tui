# frozen_string_literal: true

module EventView
  NOTHING = 0
  LEFT_KEY = 1
  RIGHT_KEY = 2
  UP_KEY = 3
  DOWN_KEY = 4
  ENTER_KEY = 5
end
