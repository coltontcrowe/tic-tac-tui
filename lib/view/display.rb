# frozen_string_literal: true

require 'curses'
require_relative '../helper/coordinate'

# Wrapper class for window display
class Display
  def initialize
    Curses.noecho
    Curses.init_screen
    @win = Curses::Window.new(9, 9, 0, 0)
    @win.nodelay = true
    @win.keypad = true
    root = File.expand_path('../../', File.dirname(__FILE__))
    ui = File.expand_path('ui/improved.txt', root)
    draw_file(ui)
  end

  def render
    @win.refresh
  end

  def getch
    @win.getch
  end

  def draw(symbol, position)
    @win.setpos(position.y, position.x)
    @win.addch(symbol)
  end

  def announce(message)
    @win.setpos(0, 0)
    @win.addstr(message)
    render
    @win.nodelay = false
    @win.getch
    @win.nodelay = true
  end

  def cursor(position)
    @win.setpos(position.y, position.x)
  end

  def draw_player(symbol)
    @win.setpos(8, 0)
    @win.addstr("Cur: #{symbol}")
    @win.setpos(2, 2)
  end

  private

  def draw_file(fname)
    origin_x = 0
    origin_y = 0
    File.foreach(fname).with_index do |line, num|
      @win.setpos(origin_y + num, origin_x)
      @win.addstr(line)
    end
  end
end
