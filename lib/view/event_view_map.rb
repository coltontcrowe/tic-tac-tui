# frozen_string_literal: true

require 'curses'
require_relative '../event_view'

# A mapping of curses events to generic view events
EVENT_VIEW_MAP = {
  Curses::Key::LEFT => EventView::LEFT_KEY,
  Curses::Key::RIGHT => EventView::RIGHT_KEY,
  Curses::Key::UP => EventView::UP_KEY,
  Curses::Key::DOWN => EventView::DOWN_KEY,
  10 => EventView::ENTER_KEY
}.freeze
