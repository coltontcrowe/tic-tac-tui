# frozen_string_literal: true

require_relative 'event_view_map'
require_relative '../event_view'
require_relative 'display'

# Encapsulates all aspects of the view being used, for talking with presenter
class View
  def initialize
    @display = Display.new
    @display.render
  end

  def parse_event
    display_input = @display.getch
    if EVENT_VIEW_MAP.key?(display_input)
      EVENT_VIEW_MAP[display_input]
    else
      EventView::NOTHING
    end
  end

  def update_cursor(position)
    @display.cursor(to_view_coord(position))
    @display.render
  end

  def update(board)
    board.x_pos.each { |coord| @display.draw('x', to_view_coord(coord)) }
    board.o_pos.each { |coord| @display.draw('o', to_view_coord(coord)) }
    @display.render
  end

  def display_winner(state)
    return if state == :in_progress

    case state
    when :x_win
      @display.announce('x wins')
    when :o_win
      @display.announce('o wins')
    when :tie
      @display.announce('tie game')
    end
  end

  private

  def to_view_coord(model_coord)
    view_y = 2 * model_coord.y + 2
    view_x = 2 * model_coord.x + 2
    Coordinate.new(view_y, view_x)
  end
end
