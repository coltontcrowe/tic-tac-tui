# frozen_string_literal: true

require_relative '../helper/coordinate'

# A cursor to track player movement
class Cursor
  attr_reader :position

  def initialize(board)
    @position = Coordinate.new(0, 0)
    @board = board
  end

  def move_up
    new_position = @position + Coordinate.new(-1, 0)
    @position = new_position if @board.valid_cursor_pos?(new_position)
  end

  def move_down
    new_position = @position + Coordinate.new(1, 0)
    @position = new_position if @board.valid_cursor_pos?(new_position)
  end

  def move_left
    new_position = @position + Coordinate.new(0, -1)
    @position = new_position if @board.valid_cursor_pos?(new_position)
  end

  def move_right
    new_position = @position + Coordinate.new(0, 1)
    @position = new_position if @board.valid_cursor_pos?(new_position)
  end
end
