# frozen_string_literal: true

require 'set'
require_relative '../helper/errors'
require_relative '../helper/coordinate'
require_relative '../presenter/map_parser'

# The Tic-tac-toe Game Board
class Board
  attr_reader :x_pos, :o_pos

  def initialize
    map_parser = MapParser.new('ui/markup.txt')
    @x_pos = map_parser.x_pos
    @o_pos = map_parser.o_pos
    @full_board = map_parser.positions
    @win_states = map_parser.win_states(3)
  end

  def state
    if x_win?
      :x_win
    elsif o_win?
      :o_win
    elsif used_spaces == @full_board
      :tie
    else
      :in_progress
    end
  end

  def valid_cursor_pos?(coord)
    @full_board.include?(coord)
  end

  def make_selection(position, symbol)
    raise InvalidMove if used_spaces.include?(position)

    symbol_list(symbol).add(position)
  end

  private

  def used_spaces
    @x_pos.union(@o_pos)
  end

  def symbol_list(symbol)
    case symbol
    when 'x'
      @x_pos
    when 'o'
      @o_pos
    else
      raise InvalidSymbol
    end
  end

  def x_win?
    @win_states.any? { |win_state| win_state.subset?(@x_pos) }
  end

  def o_win?
    @win_states.any? { |win_state| win_state.subset?(@o_pos) }
  end
end
