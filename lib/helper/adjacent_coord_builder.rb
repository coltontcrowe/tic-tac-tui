# frozen_string_literal: true

require 'set'
require_relative 'coordinate'

# Create sets of adjacent coordinates from 2 vectors
class AdjacentCoordBuilder
  def initialize(position, length)
    @pos = position
    @length = length
  end

  def row
    (0...@length).each_with_object(Set[]) do |dist, row|
      new_pos = @pos + Coordinate.new(0, dist)
      row.add(new_pos)
    end
  end

  def col
    (0...@length).each_with_object(Set[]) do |dist, col|
      new_pos = @pos + Coordinate.new(dist, 0)
      col.add(new_pos)
    end
  end

  def right_diag
    (0...@length).each_with_object(Set[]) do |dist, diag|
      new_pos = @pos + Coordinate.new(dist, dist)
      diag.add(new_pos)
    end
  end

  def left_diag
    (0...@length).each_with_object(Set[]) do |dist, diag|
      new_pos = @pos + Coordinate.new(dist, -dist)
      diag.add(new_pos)
    end
  end
end
