# frozen_string_literal: true

# Errors for the TicTacTui program
module TicTacTuiError
  # An error for commands that don't end a turn
  class NoUpdate < RuntimeError
  end

  # An error to reject invalid player moves
  class InvalidMove < RuntimeError
  end
end
