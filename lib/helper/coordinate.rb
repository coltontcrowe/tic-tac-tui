# frozen_string_literal: true

# A (y, x) coordinate in 2d space.
# y = 0 is at the top of the page
# x = 0 is at the left of the page
class Coordinate
  attr_reader :y, :x

  # :reek:Uncommunicative-Variable-Name
  def initialize(pos_y, pos_x)
    @y = pos_y
    @x = pos_x
  end

  def +(other)
    new_y = @y + other.y
    new_x = @x + other.x
    Coordinate.new(new_y, new_x)
  end

  def -(other)
    new_y = @y - other.y
    new_x = @x - other.x
    Coordinate.new(new_y, new_x)
  end

  def eql?(other)
    self.class == other.class && state == other.state
  end
  alias == eql?

  def hash
    state.reduce(0) { |acc, attr| acc ^ attr }
  end

  protected

  def state
    [@y, @x]
  end
end
