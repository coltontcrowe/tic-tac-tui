# frozen_string_literal: true

require_relative '../../interfaces/game_state'
require_relative '../../helper/errors'
require_relative '../../model/cursor'
require_relative '../command_model'
require_relative '../../event_view'
require_relative 'game_state_turn_human_map'

# Game State to handle a human player's turn
class GameStateTurnHuman < GameState
  attr_reader :cursor, :board, :player

  def initialize(engine, player, board)
    super()
    @engine = engine
    @player = player
    @board = board
    @cursor = Cursor.new(board)
  end

  def process_command
    # handle all logic here temporarily
    command = parse_input
    begin
      command.execute
    rescue TicTacTuiError::InvalidMove, TicTacTuiError::NoUpdate
      # don't need to do anything
    else
      # return to gameplay state
      @engine.pop_state
    end
  end

  def update; end

  def draw
    # draw the cursor
    # draw the new move
  end

  private

  def parse_input
    view = @engine.view
    event = view.parse_event
    if COMMAND_MODEL_MAP.key?(event)
      command = COMMAND_MODEL_MAP[event]
      command.new(self, view)
    else
      CommandModel::CommandNone.new
    end
  end
end
