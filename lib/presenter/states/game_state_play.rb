# frozen_string_literal: true

require_relative '../../interfaces/game_state'
require_relative 'game_state_turn_human'
require_relative '../../model/board'

# Game State to handle normal behavior during gameplay
class GameStatePlay < GameState
  def initialize(engine)
    super()
    @engine = engine
    @board = Board.new
    @player_current = 'o'
  end

  def process_command
    # do nothing yet
  end

  def update
    if @board.state == :in_progress
      switch_player
      player_turn = GameStateTurnHuman.new(@engine, @player_current, @board)
      @engine.push_state(player_turn)
    else
      @engine.view.display_winner(@board.state)
      @engine.pop_state
    end
  end

  def draw
    # draw the board (this is not necessary yet)
    # draw the current player
  end

  private

  def switch_player
    @player_current = case @player_current
                      when 'x' then 'o'
                      else 'x'
                      end
  end
end
