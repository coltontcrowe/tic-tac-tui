# frozen_string_literal: true

# Behavior for when gameplay is paused
class GameStatePaused < GameState
  # pause menu
  # display
end
