# frozen_string_literal: true

require_relative '../command_model'
require_relative '../../event_view'

COMMAND_MODEL_MAP = {
  EventView::LEFT_KEY => CommandModel::CommandCursorLeft,
  EventView::RIGHT_KEY => CommandModel::CommandCursorRight,
  EventView::UP_KEY => CommandModel::CommandCursorUp,
  EventView::DOWN_KEY => CommandModel::CommandCursorDown,
  EventView::ENTER_KEY => CommandModel::CommandSelect
}.freeze
