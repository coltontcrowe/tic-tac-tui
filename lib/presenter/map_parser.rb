# frozen_string_literal: true

require 'set'
require_relative '../helper/coordinate'
require_relative 'win_state_parser'

# Reads map data and determines relevant details
class MapParser
  attr_reader :x_pos, :o_pos, :positions

  BOUNDARY = (0x2500..0x257F).to_set
  def initialize(fname)
    @positions = Set[]
    @x_pos = Set[]
    @o_pos = Set[]

    root = File.expand_path('../../', File.dirname(__FILE__))
    @map_file = File.expand_path(fname, root)
    read_file
  end

  def read_file
    File.foreach(@map_file).with_index do |line, disp_y|
      process_line(line, disp_y)
    end
  end

  def win_states(length)
    WinStateParser.new(@positions, length).win_states
  end

  def unused
    used = @x_pos.union(@o_pos)
    @positions.difference(used)
  end

  private

  def process_line(line, disp_y)
    line.split('').each_with_index do |char, disp_x|
      map_y = (disp_y - 1) / 2
      map_x = (disp_x - 1) / 2
      pos = Coordinate.new(map_y, map_x)
      @positions.add(pos) if char == '#'
      @x_pos.add(pos) if char == 'x'
      @o_pos.add(pos) if char == 'o'
    end
  end
end
