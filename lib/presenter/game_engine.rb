# frozen_string_literal: true

require_relative 'states/game_state_play'
require_relative '../view/view'

# State machine for various game states
class GameEngine
  attr_reader :view

  def initialize
    @states = [GameStatePlay.new(self)]
    @view = View.new
  end

  def run
    loop do
      process_command
      update
      break unless running

      draw
    end
  end

  def push_state(new_state)
    @states.push(new_state)
  end

  def pop_state
    @states.pop
  end

  def change_state(new_state)
    @states.pop
    @states.push(new_state)
  end

  def process_command
    @states.last.process_command
  end

  def update
    @states.last.update
  end

  def draw
    @states.last.draw
  end

  def running
    !@states.empty?
  end
end
