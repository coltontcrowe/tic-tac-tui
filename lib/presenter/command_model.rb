# frozen_string_literal: true

require_relative '../interfaces/command'
require_relative '../helper/errors'

# Provides all commands from presenter to model
module CommandModel
  # Command cursor up and render
  class CommandCursorUp < Command
    def initialize(control, view)
      super()
      @control = control
      @view = view
    end

    def execute
      cursor = @control.cursor
      cursor.move_up
      @view.update_cursor(cursor.position)
      raise TicTacTuiError::NoUpdate
    end
  end

  # Command cursor down and render
  class CommandCursorDown < Command
    def initialize(control, view)
      super()
      @control = control
      @view = view
    end

    def execute
      cursor = @control.cursor
      cursor.move_down
      @view.update_cursor(cursor.position)
      raise TicTacTuiError::NoUpdate
    end
  end

  # Command cursor left and render
  class CommandCursorLeft < Command
    def initialize(control, view)
      super()
      @control = control
      @view = view
    end

    def execute
      cursor = @control.cursor
      cursor.move_left
      @view.update_cursor(cursor.position)
      raise TicTacTuiError::NoUpdate
    end
  end

  # Command cursor right and render
  class CommandCursorRight < Command
    def initialize(control, view)
      super()
      @control = control
      @view = view
    end

    def execute
      cursor = @control.cursor
      cursor.move_right
      @view.update_cursor(cursor.position)
      raise TicTacTuiError::NoUpdate
    end
  end

  # Select current cursor position as move
  class CommandSelect < Command
    def initialize(control, view)
      super()
      @control = control
      @view = view
    end

    def execute
      board = @control.board
      board.make_selection(@control.cursor.position, @control.player)
      @view.update(board)
    end
  end

  # No command provided
  class CommandNone < Command
    def execute
      raise TicTacTuiError::NoUpdate
    end
  end
end
