# frozen_string_literal: true

require 'set'
require_relative '../helper/adjacent_coord_builder'
require_relative '../helper/coordinate'

# Determines the possible win states given positions on a board
class WinStateParser
  attr_reader :win_states

  def initialize(positions, length)
    @positions = positions
    @length = length
    @win_states = Set[]
    check_each
  end

  private

  def check_each
    @positions.each do |pos|
      check_at(pos)
    end
  end

  def check_at(coord)
    adj_coord_builder = AdjacentCoordBuilder.new(coord, @length)
    check_row(adj_coord_builder.row)
    check_col(adj_coord_builder.col)
    check_right_diag(adj_coord_builder.right_diag)
    check_left_diag(adj_coord_builder.left_diag)
  end

  def check_row(row)
    @win_states.add(row) if row <= @positions
  end

  def check_col(col)
    @win_states.add(col) if col <= @positions
  end

  def check_right_diag(right_diag)
    @win_states.add(right_diag) if right_diag <= @positions
  end

  def check_left_diag(left_diag)
    @win_states.add(left_diag) if left_diag <= @positions
  end
end
