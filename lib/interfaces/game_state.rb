# frozen_string_literal: true

# Interface class for game state
class GameState
  def on_entry
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def on_exit
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def process_command
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def update
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def draw
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end
