# frozen_string_literal: true

require_relative '../helper/errors'

# Generic command interface
class Command
  def execute
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end
