# frozen_string_literal: true

require_relative 'lib/presenter/game_engine'

GameEngine.new.run
