# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/model/model_board'

RSpec.describe 'Board Tests' do
  describe 'movement' do
    let!(:board) { Board.new }
    it 'initial position' do
      expect(board.cursor).to eq(0)
    end

    it 'move right one' do
      board.move_cursor_right
      expect(board.cursor).to eq(1)
    end

    it 'move down one' do
      board.move_cursor_down
      expect(board.cursor).to eq(3)
    end
  end
end
